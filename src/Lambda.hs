{-# LANGUAGE LambdaCase #-}

module Lambda
    ( runEval
    , freeVars
    ) where

import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Except

import           Control.Monad.Trans.State
import qualified Data.Set                      as Set
import           Term                           ( Term(..) )

type Fresh = State Int

-- get a fresh name
getFresh :: Fresh String
getFresh = do
    i <- get
    put (i + 1)
    pure $ show i

-- alpha conversion
alpha :: String -> Term -> Fresh Term
alpha v t = do
    nm <- getFresh
    pure $ Abs nm (subname t v nm)

-- name substitution
subname :: Term -> String -> String -> Term
subname t o x = case t of
    Var n | n == o   -> Var x
    Abs v t | v /= o -> Abs v (subname t o x)
    App t1 t2        -> App (subname t1 o x) (subname t2 o x)
    _                -> t

-- check free variables
freeVars :: Term -> Set.Set String
freeVars (Var m) = Set.singleton m
freeVars (Abs v t) =
    let st = freeVars t in if Set.member v st then Set.delete v st else st
freeVars (App t1 t2) = freeVars t1 `Set.union` freeVars t2

-- term substitution
subst :: String -> Term -> Term -> Fresh Term
subst x s t@(Var n    ) = pure $ if n == x then s else t
subst x s t@(App t1 t2) = App <$> subst x s t1 <*> subst x s t2
subst x s t@(Abs v tm) | v == x                    = pure t
                       | Set.member v (freeVars s) = subst x s =<< alpha v tm
                       | otherwise                 = Abs v <$> subst x s tm

-- eval one step
eval :: Term -> Fresh (Maybe Term)
eval t@(Var _               ) = pure Nothing
eval t@(Abs v             tm) = fmap (Abs v) <$> eval tm
eval t@(App t1@(Var _   ) t2) = fmap (App t1) <$> eval t2
eval t@(App t1@(Abs v tm) t2) = Just <$> subst v t2 tm
eval t@(App t1@App{}      t2) = eval t1 >>= \case
    Just t' -> pure . Just $ App t' t2
    Nothing -> fmap (App t1) <$> eval t2

-- term, max steps, traced, hold
runEval :: Term -> Integer -> Bool -> Bool -> Maybe [Term]
runEval tm _ _     True = Just [tm]
runEval tm n True  _    = runEvalTraced tm n
runEval tm n False _    = pure <$> runEvalLast tm n

runEvalTraced :: Term -> Integer -> Maybe [Term]
runEvalTraced tm n = loop tm n 0
  where
    loop tm 0 _ = Nothing
    loop tm n i = case runState (eval tm) i of
        (Nothing, _ ) -> Just [tm]
        (Just te, i') -> (tm :) <$> loop te (n - 1) i'

runEvalLast :: Term -> Integer -> Maybe Term
runEvalLast tm n = loop tm n 0
  where
    loop tm 0 _ = Nothing
    loop tm n i = case runState (eval tm) i of
        (Nothing, _ ) -> Just tm
        (Just te, i') -> loop te (n - 1) i'

-- new impl

type Eval = ExceptT String (State Int)

eval' :: Term -> Eval Term
eval' t@(Var _               ) = throwE "Already a value"
eval' t@(Abs v             tm) = Abs v <$> eval' tm
eval' t@(App t1@(Var _   ) t2) = App t1 <$> eval' t2
eval' t@(App t1@(Abs v tm) t2) = (except . Right) =<< lift (subst v t2 tm)
eval' t@(App t1@App{} t2) =
    catchE ((`App` t2) <$> eval' t1) $ \_ -> App t1 <$> eval' t2
