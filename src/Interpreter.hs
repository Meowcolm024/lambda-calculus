module Interpreter where

import           Control.Monad                  ( forM_ )
import           Data.Functor                   ( ($>) )
import           Data.List                      ( intercalate )
import qualified Data.Map                      as Map
import           Data.Maybe                     ( fromMaybe )
import qualified Data.Set                      as Set
import           Lambda
import           Parser
import           System.Exit                    ( exitSuccess )
import           System.IO
import           Term
import           Text.Read                      ( readMaybe )

data Env = Env
    { env   :: Map.Map String Term  -- ^ lambda bindings
    , trace :: Bool                 -- ^ show intermediate steps
    , step  :: Integer              -- ^ max eval steps
    , hold  :: Bool                 -- ^ hold evaluation
    }

interpret :: Env -> Lambda -> IO Env
interpret m@(Env e tr st hl) lm = case lm of
    Def s te           -> handleDef m s te
    Opt (Settings opt) -> handleSet m opt
    Opt List           -> handleLst m $> m
    Opt Help           -> handleHlp $> m
    Opt Quit           -> exitSuccess
    Raw te             -> handleRaw m te $> m

handleRaw :: Env -> Term -> IO ()
handleRaw m@(Env e tr st hl) te =
    let maxMsg = hPutStrLn stderr "[error] max evaluation steps reached"
    in  maybe maxMsg (mapM_ print) (runEval (expand e te) st tr hl)

handleDef :: Env -> String -> Term -> IO Env
handleDef m@(Env e _ _ _) s te
    | Map.member s e = msg m $ s ++ " is already defined"
    | Set.member s fv = msg m $ s ++ " can't be recursively defined"
    | not (Set.null fv) = msg m $ concat
        [s, " has free variable(s): ", intercalate ", " (Set.toList fv)]
    | otherwise = pure $ m { env = Map.insert s ex e }
  where
    ex = expand e te
    fv = freeVars ex

handleLst :: Env -> IO ()
handleLst m = forM_ (Map.toList (env m))
    $ \(b, l) -> putStrLn $ "  " ++ b ++ " = " ++ show l

handleSet :: Env -> [String] -> IO Env
handleSet m ["trace", "true" ] = pure $ m { trace = True }
handleSet m ["trace", "false"] = pure $ m { trace = False }
handleSet m ["hold" , "true" ] = pure $ m { hold = True }
handleSet m ["hold" , "false"] = pure $ m { hold = False }
handleSet m ["step" , n      ] = maybe (msg m $ "invalid step: " ++ n)
                                       (\n -> pure $ m { step = n })
                                       (readMaybe n)
handleSet m ["reset"] = pure initEnv
handleSet m opt       = msg m $ "invalid option(s): " ++ unwords opt

handleHlp :: IO ()
handleHlp = mapM_
    putStrLn
    [ "options"
    , " :help\t\t---- show this help"
    , " :list\t\t---- list all bindings"
    , " :quit\t\t---- quit program"
    , " :set [opt] [n]\t---- settings"
    , "   [opt] = trace | hold  | step | reset"
    , "   [n]   = true  | false | <number>"
    ]

-- initial environment
initEnv :: Env
initEnv = Env { env = primitives, trace = False, step = 4096, hold = False }

-- print error messages
msg :: Env -> String -> IO Env
msg m x = hPutStrLn stderr ("[error] " ++ x) $> m

-- expand bindings
expand :: Map.Map String Term -> Term -> Term
expand e t@(Var n    ) = fromMaybe t (Map.lookup n e)
expand e t@(App t1 t2) = App (expand e t1) (expand e t2)
expand e t@(Abs n  tm) = if Map.member n e
    then Abs n (expand (Map.delete n e) tm)
    else Abs n (expand e tm)

-- primitive bindings
primitives :: Map.Map String Term
primitives = maybe Map.empty Map.fromList $ traverse helper =<< rightToMaybe
    (mapM
        (regularParse defOrlam)
        [ "Y = \\g.(\\x.g (\\v.x x v)) (\\x.g (\\v.x x v))"
        , "id = \\x.x"
        , "true = \\x.\\y.x"
        , "false = \\x.\\y.y"
        , "and = \\p.\\q.p q p"
        , "or = \\p.\\q.p p q"
        , "not = \\p.\\a.\\b.p b a"
        , "if = \\p.\\a.\\b.p a b"
        , "zero = \\f.\\x.x"
        , "one = \\f.\\x.f x"
        , "two = \\f.\\x.f (f x)"
        , "three = \\f.\\x.f (f (f x))"
        , "succ = \\n.\\f.\\x.f (n f x)"
        , "pred = \\n.\\f.\\x.n (\\g.\\h.h (g f)) (\\u.x) \\u.u"
        , "add = \\n.\\m.\\f.\\x.n f (m f x)"
        , "sub = \\n.\\m.m(\\n.\\f.\\x.n(\\g.\\h.h(g f))(\\u.x)(\\u.u)) n"
        , "mult = \\n.\\m.\\f.n (m f)"
        , "iszero = \\n.n (\\x.\\x.\\y.y) \\x.\\y.x"
        ]
    )
  where
    helper (Def n t) = Just (n, t)
    helper _         = Nothing
    rightToMaybe (Left  _) = Nothing
    rightToMaybe (Right a) = Just a
