# Lambda Calculus

Lambda Calculus interpreter with **normal evaluation order**.

## Build

To build to project:

```sh
$ stack build
```

and to run:

```sh
$ stack run
Lambda Calculus
> 
```

## Usage

Excample usage:

```txt
> fact = Y (\f.\x. if (iszero x) one (mult x (f (pred x))))
> fact three
λf.λx.f(f(f(f(f(f x)))))
```

Showing intermediate steps:

```txt
> :set trace true
> succ one
(λn.λf.λx.f(n f x))(λf.λx.f x)
λf.λx.f((λf.λx.f x) f x)
λf.λx.f((λx.f x) x)
λf.λx.f(f x)
```
