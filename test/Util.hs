module Util where

import           Interpreter
import           Parser
import           System.IO.Silently
import           Term                           ( Lambda )
import           Test.Hspec

parse :: String -> Lambda
parse s = case regularParse defOrlam s of
    Right lam -> lam
    Left  err -> error (show err)

lamEq :: Env -> String -> String -> IO ()
lamEq env l1 l2 = do
    (r1, _) <- capture $ interpret env (parse l1)
    (r2, _) <- capture $ interpret env (parse l2)
    r1 `shouldBe` r2

(===) :: String -> String -> IO ()
(===) = lamEq initEnv
