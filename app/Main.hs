module Main where

import           Control.Monad                  ( when )
import           Data.Char                      ( isSpace )
import           Interpreter
import           Parser
import           System.Exit                    ( exitSuccess )
import           System.IO

main :: IO ()
main = putStrLn "Lambda Calculus" *> loop initEnv

loop :: Env -> IO ()
loop e = do
    putStr "> " *> hFlush stdout
    isEOF >>= (`when` exitSuccess)  -- exit when ^D
    x <- getLine
    if all isSpace x
        then loop e
        else case regularParse defOrlam x of
            Left  err -> hPrint stderr err *> loop e
            Right lam -> interpret e lam >>= loop
