import           Interpreter
import           System.IO.Silently
import           Test.Hspec
import           Util

main :: IO ()
main = do
    hspec $ describe "basic ops" $ do
        it "id id == id" $ "id id" === "id"
        it "succ one == two" $ "succ one" === "two"
        it "pred zero == zero" $ "pred zero" === "zero"
        it "and true false == false" $ "and true false" === "false"
        it "or true false == true" $ "or true false" === "true"
    hspec $ describe "complex ops" $ do
        let fact =
                "fact = Y (\\f.\\x. if (iszero x) one (mult x (f (pred x))))"
        it "fact zero == one" $ do
            env <- silence $ interpret initEnv (parse fact)
            lamEq env "fact zero" "one"
        it "fact three == add three three" $ do
            env <- silence $ interpret initEnv (parse fact)
            lamEq env "fact three" "add three three"
    hspec $ describe "trivial ops" $ do
        it "(\\one.one) a == id a" $ "(\\one.one) a" === "id a"
        it "(\\one. one two) id == two" $ "(\\one. one two) id" === "two"
        it "(\\one.\\two. if true one two) a b == a"
            $   "(\\one.\\two. if true one two) a b"
            === "a"
    hspec $ describe "option tests" $ do
        it "last of traced should be output" $ do
            traced    <- silence $ interpret initEnv (parse ":set trace true")
            (out1, _) <- capture $ interpret traced (parse "pred two")
            (out2, _) <- capture $ interpret initEnv (parse "pred two")
            last (lines out1) `shouldBe` last (lines out2)
        it "first of traced should be hold" $ do
            traced    <- silence $ interpret initEnv (parse ":set trace true")
            (out1, _) <- capture $ interpret traced (parse "succ two")
            holded    <- silence $ interpret initEnv (parse ":set hold true")
            (out2, _) <- capture $ interpret holded (parse "succ two")
            head (lines out1) `shouldBe` head (lines out2)
