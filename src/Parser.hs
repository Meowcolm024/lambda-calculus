module Parser
    ( regularParse
    , defOrlam
    ) where

import           Data.Char                      ( isAlpha )
import           Data.Functor                   ( ($>) )
import           Data.Functor.Identity          ( Identity )
import           Term
import           Text.Parsec
import           Text.Parsec.Language           ( emptyDef )
import           Text.Parsec.String             ( Parser )
import qualified Text.Parsec.Token             as P

lexer :: P.GenTokenParser String u Identity
lexer = P.makeTokenParser emptyDef
    { P.identStart  = satisfy (\p -> isAlpha p && (p /= 'λ'))
    , P.identLetter = alphaNum
    }

ident :: Parser String
ident = P.identifier lexer

parens :: Parser a -> Parser a
parens = P.parens lexer

op :: String -> Parser ()
op s = spaces *> string s *> spaces

regularParse :: Parser a -> String -> Either ParseError a
regularParse p = parse p ""

defOrlam :: Parser Lambda
defOrlam = settings <|> do
    e <- expr
    b <- optionMaybe (op "=" *> expr)
    eof
    case (e, b) of
        (_    , Nothing) -> pure $ Raw e
        (Var n, Just te) -> pure $ Def n te
        _                -> fail "invalid syntax"

expr :: Parser Term
expr = (factor <|> lambda) `chainl1` pure App

lambda :: Parser Term
lambda = Abs <$> ((op "\\" <|> op "λ") *> ident <* op ".") <*> expr

factor :: Parser Term
factor = Var <$> ident <|> parens expr

settings :: Parser Lambda
settings = do
    char ':'
    opt <- choice
        [ Settings
            <$> (string "set" *> spaces *> sepBy1 (many1 alphaNum) spaces)
        , string "list" *> spaces $> List
        , string "quit" *> spaces $> Quit
        , string "help" *> spaces $> Help
        ]
    eof
    pure $ Opt opt
