module Term where

data Term = Var String          -- ^ variable
          | Abs String Term     -- ^ lambda abstraction λv . t
          | App Term Term       -- ^ lambda application f x

-- stolen from https://crypto.stanford.edu/~blynn/lambda/
instance Show Term where
  show (Abs s t) = "λ" ++ s ++ "." ++ show t
  show (Var s  ) = s
  show (App x y) = showL x ++ showR y   where
    showL (Abs _ _) = "(" ++ show x ++ ")"
    showL _         = show x
    showR (Var s) = ' ' : s
    showR _       = "(" ++ show y ++ ")"

data Lambda = Def String Term     -- ^ lambda bindings
            | Raw Term            -- ^ raw lambda term
            | Opt Option          -- ^ options
            deriving Show

data Option = Settings [String] | List | Help | Quit deriving Show
